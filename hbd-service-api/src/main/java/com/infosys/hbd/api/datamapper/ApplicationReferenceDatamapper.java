/**
 * 
 */
package com.infosys.hbd.api.datamapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.infosys.hbd.api.constants.DatabaseColumnConstants;
import com.infosys.hbd.api.dto.ApplicationReferenceDto;

/**
 * @author Barath
 *
 */
public class ApplicationReferenceDatamapper implements RowMapper<ApplicationReferenceDto> {

	@Override
	public ApplicationReferenceDto mapRow(ResultSet rs, int rowNum) throws SQLException {
		ApplicationReferenceDto applicationReferenceDto = new ApplicationReferenceDto();
		applicationReferenceDto.setReferenceKey(rs.getString(DatabaseColumnConstants.REFERENCE_KEY));
		applicationReferenceDto.setReferenceType(rs.getString(DatabaseColumnConstants.REFERENCE_TYPE));
		applicationReferenceDto.setReferenceValue(rs.getString(DatabaseColumnConstants.REFERENCE_VALUE));
		return applicationReferenceDto;
	}

}
