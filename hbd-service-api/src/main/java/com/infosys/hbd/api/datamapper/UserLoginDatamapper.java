/**
 * 
 */
package com.infosys.hbd.api.datamapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.infosys.hbd.api.constants.DatabaseColumnConstants;
import com.infosys.hbd.api.dto.UserLoginDto;

/**
 * @author Barath
 *
 */
public class UserLoginDatamapper implements RowMapper<UserLoginDto> {

	@Override
	public UserLoginDto mapRow(ResultSet rs, int rowNum) throws SQLException {
		UserLoginDto userLoginDto = new UserLoginDto();
		userLoginDto.setUserName(rs.getString(DatabaseColumnConstants.USER_NAME));
		userLoginDto.setUserPassword(rs.getString(DatabaseColumnConstants.USER_PASSWORD));
		userLoginDto.setUserType(rs.getString(DatabaseColumnConstants.USER_TYPE));
		return userLoginDto;
	}

}
