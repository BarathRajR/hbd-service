/**
 * 
 */
package com.infosys.hbd.api.constants;

/**
 * @author Barath
 *
 */
public final class DatabaseColumnConstants {
	public static final String REFERENCE_TYPE = "REFERENCE_TYPE";
	public static final String REFERENCE_KEY = "REFERENCE_KEY";
	public static final String REFERENCE_VALUE = "REFERENCE_VALUE";
	
	public static final String USER_NAME = "USER_NAME";
	public static final String USER_TYPE = "USER_TYPE";
	public static final String USER_PASSWORD ="USER_PASSWORD";
	
	public static final String USER_EMAIL_ID ="USER_EMAIL_ID";
	public static final String EMPLOYEE_NUMBER ="EMPLOYEE_NUMBER";
	public static final String HOBBIES = "HOBBIES";
	public static final String EMPLOYEE_ROLE = "EMPLOYEE_ROLE";
	public static final String YEARS_IN_TEAM = "YEARS_IN_TEAM";

}
