/**
 * 
 */
package com.infosys.hbd.api.model;

import java.util.Map;

/**
 * @author Barath
 *
 */
public class ApplicationReference {
	private String reference;
	private Map<String, String> referenceDetails;

	/**
	 * @return the reference
	 */
	public String getReference() {
		return reference;
	}

	/**
	 * @param reference the reference to set
	 */
	public void setReference(String reference) {
		this.reference = reference;
	}

	/**
	 * @return the referenceDetails
	 */
	public Map<String, String> getReferenceDetails() {
		return referenceDetails;
	}

	/**
	 * @param referenceDetails the referenceDetails to set
	 */
	public void setReferenceDetails(Map<String, String> referenceDetails) {
		this.referenceDetails = referenceDetails;
	}

}
