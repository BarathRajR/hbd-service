/**
 * 
 */
package com.infosys.hbd.api.dto;

/**
 * @author Barath
 *
 */
public class UserDetailsDto {
	private String userName;
	private String userEmailId;
	private String employeeNo;
	private String hobbies;
	private String employeeRole;
	private Double noOfYearsInTeam;
	private String password;

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @param employeeNo
	 *            the employeeNo to set
	 */
	public void setEmployeeNo(String employeeNo) {
		this.employeeNo = employeeNo;
	}

	/**
	 * @return the employeeRole
	 */
	public String getEmployeeRole() {
		return employeeRole;
	}

	/**
	 * @param employeeRole
	 *            the employeeRole to set
	 */
	public void setEmployeeRole(String employeeRole) {
		this.employeeRole = employeeRole;
	}

	/**
	 * @return the noOfYearsInTeam
	 */
	public Double getNoOfYearsInTeam() {
		return noOfYearsInTeam;
	}

	/**
	 * @param noOfYearsInTeam
	 *            the noOfYearsInTeam to set
	 */
	public void setNoOfYearsInTeam(Double noOfYearsInTeam) {
		this.noOfYearsInTeam = noOfYearsInTeam;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName
	 *            the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the userEmailId
	 */
	public String getUserEmailId() {
		return userEmailId;
	}

	/**
	 * @param userEmailId
	 *            the userEmailId to set
	 */
	public void setUserEmailId(String userEmailId) {
		this.userEmailId = userEmailId;
	}

	/**
	 * @return the employeeNo
	 */
	public String getEmployeeNo() {
		return employeeNo;
	}

	/**
	 * @return the hobbies
	 */
	public String getHobbies() {
		return hobbies;
	}

	/**
	 * @param hobbies
	 *            the hobbies to set
	 */
	public void setHobbies(String hobbies) {
		this.hobbies = hobbies;
	}

}
