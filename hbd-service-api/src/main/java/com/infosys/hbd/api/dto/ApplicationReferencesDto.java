/**
 * 
 */
package com.infosys.hbd.api.dto;

import java.util.List;

/**
 * @author Barath
 *
 */
public class ApplicationReferencesDto {
	private List<ApplicationReferenceDto> applicationReference;

	/**
	 * @return the applicationReference
	 */
	public List<ApplicationReferenceDto> getApplicationReference() {
		return applicationReference;
	}

	/**
	 * @param applicationReference the applicationReference to set
	 */
	public void setApplicationReference(List<ApplicationReferenceDto> applicationReference) {
		this.applicationReference = applicationReference;
	}

}
