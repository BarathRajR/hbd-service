/**
 * 
 */
package com.infosys.hbd.api.datamapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.infosys.hbd.api.constants.DatabaseColumnConstants;
import com.infosys.hbd.api.dto.UserDetailsDto;

/**
 * @author Barath
 *
 */
public class UserDetailsDatamapper implements RowMapper<UserDetailsDto>{

	@Override
	public UserDetailsDto mapRow(ResultSet rs, int rowNum) throws SQLException {
		UserDetailsDto userDetailsDto = new UserDetailsDto();
		userDetailsDto.setEmployeeNo(rs.getString(DatabaseColumnConstants.EMPLOYEE_NUMBER));
		userDetailsDto.setEmployeeRole(rs.getString(DatabaseColumnConstants.EMPLOYEE_ROLE));
		userDetailsDto.setHobbies(rs.getString(DatabaseColumnConstants.HOBBIES));
		userDetailsDto.setNoOfYearsInTeam(rs.getDouble(DatabaseColumnConstants.YEARS_IN_TEAM));
		userDetailsDto.setUserEmailId(rs.getString(DatabaseColumnConstants.USER_EMAIL_ID));
		userDetailsDto.setUserName(rs.getString(DatabaseColumnConstants.USER_NAME));
		return null;
	}

}
