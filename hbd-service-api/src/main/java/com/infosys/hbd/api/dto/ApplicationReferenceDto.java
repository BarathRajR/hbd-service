/**
 * 
 */
package com.infosys.hbd.api.dto;

/**
 * @author Barath
 *
 */
public class ApplicationReferenceDto {
	private String referenceType;
	private String referenceKey;
	private String referenceValue;

	/**
	 * @return the referenceType
	 */
	public String getReferenceType() {
		return referenceType;
	}

	/**
	 * @param referenceType the referenceType to set
	 */
	public void setReferenceType(String referenceType) {
		this.referenceType = referenceType;
	}

	/**
	 * @return the referenceKey
	 */
	public String getReferenceKey() {
		return referenceKey;
	}

	/**
	 * @param referenceKey the referenceKey to set
	 */
	public void setReferenceKey(String referenceKey) {
		this.referenceKey = referenceKey;
	}

	/**
	 * @return the referenceValue
	 */
	public String getReferenceValue() {
		return referenceValue;
	}

	/**
	 * @param referenceValue the referenceValue to set
	 */
	public void setReferenceValue(String referenceValue) {
		this.referenceValue = referenceValue;
	}

}
