/**
 * 
 */
package com.infosys.hbd.rest.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * @author Barath
 *
 */
public abstract class GenericDatabaseUtil {
	@Autowired
	private JdbcTemplate jdbcTemplate;

	public Integer countRows(String sqlQuery, Object[] sqlParams) {
		return jdbcTemplate.queryForObject(sqlQuery, sqlParams, Integer.class);
	}

	public boolean updateInDatabase(String sqlQuery, Object[] sqlParams) {
		return jdbcTemplate.update(sqlQuery, sqlParams) > 0 ? true : false;
	}

}
