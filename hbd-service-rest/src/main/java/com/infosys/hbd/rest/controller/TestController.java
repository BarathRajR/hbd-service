/**
 * 
 */
package com.infosys.hbd.rest.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Barath
 *
 */
@RestController
public class TestController {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@RequestMapping(value = "test", method = RequestMethod.GET)
	public @ResponseBody Map<String, String> test() {
		Map<String, String> test = new HashMap<>();
		test.put("RESULT", "SUCCESS");
		int count = jdbcTemplate
				.update("INSERT INTO HBD_USERLOGIN(USER_NAME,USER_PASSWORD,USER_TYPE) VALUES('HBD','HBD','ADMIN')");
		System.out.println("ROWS UPDTED:" + count);
		return test;
	}

}
