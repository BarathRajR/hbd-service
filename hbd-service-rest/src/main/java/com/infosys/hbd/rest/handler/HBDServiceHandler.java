/**
 * 
 */
package com.infosys.hbd.rest.handler;

import org.apache.commons.lang3.StringUtils;

import com.infosys.hbd.api.dto.UserLoginDto;
import com.infosys.hbd.rest.util.HBDDatabaseUtil;

/**
 * @author Barath
 *
 */
public abstract class HBDServiceHandler extends HBDDatabaseUtil {

	public String validateLogin(String userName, String userPassword) {
		UserLoginDto userLoginDto = fetchUserLogin(userName);
		if (null != userLoginDto) {
			if (StringUtils.equals(userPassword, userLoginDto.getUserPassword())) {
				return userLoginDto.getUserType();
			}
		}
		return null;
	}

}
