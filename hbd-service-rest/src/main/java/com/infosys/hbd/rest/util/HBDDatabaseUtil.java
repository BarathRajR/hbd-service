/**
 * 
 */
package com.infosys.hbd.rest.util;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.infosys.hbd.api.constants.DatabaseQueryConstants;
import com.infosys.hbd.api.datamapper.ApplicationReferenceDatamapper;
import com.infosys.hbd.api.datamapper.UserLoginDatamapper;
import com.infosys.hbd.api.dto.ApplicationReferenceDto;
import com.infosys.hbd.api.dto.UserDetailsDto;
import com.infosys.hbd.api.dto.UserLoginDto;

/**
 * @author Barath
 *
 */
@Component
public class HBDDatabaseUtil extends GenericDatabaseUtil {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	public List<ApplicationReferenceDto> fetchApplicationReference() {
		return jdbcTemplate.query(DatabaseQueryConstants.FETCH_APPLICATION_REFERENCE,
				new ApplicationReferenceDatamapper());
	}

	public UserLoginDto fetchUserLogin(String userName) {
		Object[] sqlParam = new Object[] { userName };
		int noOfRows = countRows(DatabaseQueryConstants.COUNT_USER_NAME, sqlParam);
		if (noOfRows != 0) {
			return jdbcTemplate.queryForObject(DatabaseQueryConstants.FETCH_USER_LOGIN, sqlParam,
					new UserLoginDatamapper());
		}
		return null;
	}

	public boolean insertUserDetails(UserDetailsDto userDetailsDto) {
		Object[] sqlParams = new Object[] { userDetailsDto.getUserName(), userDetailsDto.getUserEmailId(),
				userDetailsDto.getEmployeeNo(), userDetailsDto.getHobbies(), userDetailsDto.getEmployeeRole(), 0 };
		return updateInDatabase(DatabaseQueryConstants.INSERT_USER_DETAILS, sqlParams);
	}

	public boolean insertUserLogin(UserDetailsDto userDetailsDto) {
		Object[] sqlParams = new Object[] { userDetailsDto.getUserName(), "USER", userDetailsDto.getPassword() };
		return updateInDatabase(DatabaseQueryConstants.INSERT_USER_LOGIN, sqlParams);
	}
}
