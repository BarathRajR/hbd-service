/**
 * 
 */
package com.infosys.hbd.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.infosys.hbd.api.dto.ApplicationReferencesDto;
import com.infosys.hbd.api.dto.UserDetailsDto;
import com.infosys.hbd.api.dto.UserLoginDto;
import com.infosys.hbd.rest.service.HBDService;

/**
 * This class will have all the rest service method
 * 
 * @author Barath
 *
 */
@RestController
@CrossOrigin(allowedHeaders = "*")
public class HBDController {

	@Autowired
	private HBDService hbdService;

	/**
	 * This method will fetch the application references
	 * 
	 * @return {@link ApplicationReferencesDto} - Application references object
	 */
	@CrossOrigin(allowedHeaders = "*")
	@RequestMapping(value = "applicationReference", method = RequestMethod.GET)
	public @ResponseBody ApplicationReferencesDto retriveApplicationReference() {
		return hbdService.fetchFormattedApplicationReference();
	}

	/**
	 * This method will validate the login data
	 * 
	 * @param userLoginDto
	 *            - UserLogin details
	 * @return {@link String} - The user type based on login
	 */
	@CrossOrigin(allowedHeaders = "*")
	@RequestMapping(value = "login", method = RequestMethod.POST)
	public @ResponseBody String validateLogin(@RequestBody UserLoginDto userLoginDto) {
		return hbdService.validateLogin(userLoginDto.getUserName(), userLoginDto.getUserPassword());
	}

	/**
	 * This method will add the new user to table
	 * 
	 * @param userDetailsDto
	 *            - User details
	 * @return {@link Boolean} - True, if the update is successful
	 */
	@CrossOrigin(allowedHeaders = "*")
	@RequestMapping(value = "signup", method = RequestMethod.POST)
	public @ResponseBody boolean insertUserDetails(@RequestBody UserDetailsDto userDetailsDto) {
		return hbdService.addNewUser(userDetailsDto);
	}

}
