/**
 * 
 */
package com.infosys.hbd.rest.service;

import org.springframework.stereotype.Component;

import com.infosys.hbd.api.dto.ApplicationReferencesDto;
import com.infosys.hbd.api.dto.UserDetailsDto;
import com.infosys.hbd.rest.handler.HBDServiceHandler;

/**
 * @author Barath
 *
 */
@Component
public class HBDService extends HBDServiceHandler {

	public ApplicationReferencesDto fetchFormattedApplicationReference() {
		ApplicationReferencesDto applicationReferencesDto = new ApplicationReferencesDto();
		applicationReferencesDto.setApplicationReference(fetchApplicationReference());
		return applicationReferencesDto;
	}

	public boolean addNewUser(UserDetailsDto userDetailsDto) {
		insertUserDetails(userDetailsDto);
		return insertUserLogin(userDetailsDto);
	}

}
