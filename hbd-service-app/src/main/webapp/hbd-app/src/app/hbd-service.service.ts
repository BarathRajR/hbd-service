import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable,of } from 'rxjs';


import { ApplicationReferences } from './application-references';
import { UserLogin } from './user-login';
import { UserDetails } from './user-details';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class HbdServiceService {
apiUrl = environment.apiUrl;
appReferenceUrl = this.apiUrl+'applicationReference';
userLoginUrl = this.apiUrl+'login';
signUpUrl = this.apiUrl+'signup';

  constructor(private http : HttpClient) { }
  getAllApplicationReference():Observable<ApplicationReferences>{
  	return this.http.get<ApplicationReferences>(this.appReferenceUrl);
  }
  
  validateLogin(userLogin : UserLogin): Observable<string>{
  return this.http.post<string>(this.userLoginUrl, userLogin, httpOptions);

  }
  signUp(userDetails:UserDetails): Observable<boolean>{
  return this.http.post<boolean>(this.signUpUrl, userDetails, httpOptions);

  }
}
