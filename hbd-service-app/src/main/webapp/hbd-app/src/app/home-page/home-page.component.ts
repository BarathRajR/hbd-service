import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { HbdServiceService } from '../hbd-service.service';
import { UserLogin } from '../user-login';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {

userLogin : UserLogin;
userType : string;

  constructor(private router:Router,private hbdService:HbdServiceService) { }

  ngOnInit() {
  this.userLogin = {};
  this.userType = '';
  }
  
  login(){
  this.hbdService.validateLogin(this.userLogin).subscribe(
  val => {console.log('Value emitted successfully', val);  this.router.navigate(['overview'])},
            error => {
                console.error("This line is never called ",error);
            },
            () => console.log("HTTP Observable completed...")
  );
  }
  
  signUp(){
  this.router.navigate(['user-details']);
  }

}
