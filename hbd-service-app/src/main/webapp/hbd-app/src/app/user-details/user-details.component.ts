import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { UserDetails } from '../user-details';
import { HbdServiceService } from '../hbd-service.service';
@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {
 userDetails:UserDetails;
 response : boolean;
  constructor(private hbdService:HbdServiceService,private router:Router) { }

  ngOnInit() {
  this.userDetails = {};
  }
signUp(){
this.hbdService.signUp(this.userDetails).subscribe(response => this.response=response);
this.router.navigate(['overview']);
}
}
