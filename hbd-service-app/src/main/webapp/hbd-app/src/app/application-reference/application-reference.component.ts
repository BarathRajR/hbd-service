import { Component, OnInit } from '@angular/core';

import { HbdServiceService } from '../hbd-service.service';
import { ApplicationReferences } from '../application-references';

@Component({
  selector: 'app-application-reference',
  templateUrl: './application-reference.component.html',
  styleUrls: ['./application-reference.component.css']
})
export class ApplicationReferenceComponent implements OnInit {

  constructor(private hbdService:HbdServiceService) { }

	applicationReferences :ApplicationReferences;

  ngOnInit() {
  this.getAppReferences();
  }
  
  getAppReferences(){
  	this.hbdService.getAllApplicationReference().subscribe(response=>this.applicationReferences=response);
  }
}
