import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule,HttpClient } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomePageComponent } from './home-page/home-page.component';
import { ApplicationReferenceComponent } from './application-reference/application-reference.component';
import { UserDetailsComponent } from './user-details/user-details.component';
import { TeamStructureComponent } from './team-structure/team-structure.component';
import { OverviewComponent } from './overview/overview.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { FeedbackComponent } from './feedback/feedback.component';
import { FaqComponent } from './faq/faq.component';
import { ApplicationDetailsComponent } from './application-details/application-details.component';
import { AccessRightsComponent } from './access-rights/access-rights.component';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    ApplicationReferenceComponent,
    UserDetailsComponent,
    TeamStructureComponent,
    OverviewComponent,
    NavBarComponent,
    FeedbackComponent,
    FaqComponent,
    ApplicationDetailsComponent,
    AccessRightsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
