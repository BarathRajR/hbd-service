import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomePageComponent } from './home-page/home-page.component';
import { ApplicationReferenceComponent } from './application-reference/application-reference.component';
import { UserDetailsComponent } from './user-details/user-details.component';
import { OverviewComponent } from './overview/overview.component';
import { TeamStructureComponent } from './team-structure/team-structure.component';
import { FaqComponent } from './faq/faq.component';
import { FeedbackComponent } from './feedback/feedback.component';
import { ApplicationDetailsComponent } from './application-details/application-details.component';
import { AccessRightsComponent } from './access-rights/access-rights.component';

const routes: Routes = [
{ path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomePageComponent },
  { path: 'application-reference', component: ApplicationReferenceComponent },
  { path: 'user-details', component: UserDetailsComponent },
  { path: 'overview', component: OverviewComponent },
  { path: 'team',component:TeamStructureComponent},
  { path: 'faq', component:FaqComponent},
  { path: 'feedback',component:FeedbackComponent},
  { path: 'application-details', component:ApplicationDetailsComponent},
  { path: 'access-rights',component:AccessRightsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
