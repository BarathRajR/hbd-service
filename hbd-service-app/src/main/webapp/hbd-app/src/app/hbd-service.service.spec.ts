import { TestBed } from '@angular/core/testing';

import { HbdServiceService } from './hbd-service.service';

describe('HbdServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HbdServiceService = TestBed.get(HbdServiceService);
    expect(service).toBeTruthy();
  });
});
