/**
 * 
 */
package com.infosys.hbd.config;

import java.util.List;

import javax.sql.DataSource;
import javax.xml.crypto.Data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

/**
 * @author Barath
 *
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.infosys.hbd")
public class ApplicationConfig implements WebMvcConfigurer {

	@Autowired
	@Qualifier("dbDataSource")
	private DataSource dataSource;

	@Bean
	public ViewResolver viewResolver() {
		InternalResourceViewResolver internalResourceViewResolver = new InternalResourceViewResolver();
		internalResourceViewResolver.setViewClass(JstlView.class);
		internalResourceViewResolver.setPrefix("/WEB-INF/*");
		internalResourceViewResolver.setSuffix("*.jsp");
		return internalResourceViewResolver;
	}

	public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
		converters.add(new MappingJackson2HttpMessageConverter());
	}

	/*
	 * @Bean public DataSource dataSource() { DriverManagerDataSource
	 * driverManagerDataSource = new DriverManagerDataSource();
	 * driverManagerDataSource.setUrl("jdbc:oracle:thin:@10.122.152.56:1521:xe");
	 * driverManagerDataSource.setUsername("hbd_team");
	 * driverManagerDataSource.setPassword("abc");
	 * driverManagerDataSource.setDriverClassName("oracle.jdbc.driver.OracleDriver")
	 * ; return driverManagerDataSource; }
	 */

	@Bean
	public JdbcTemplate jdbcTemplate() {
		return new JdbcTemplate(dataSource);
	}
}
